# hwsc/ci-go Image
`hwsc/ci-go` image is used to run GitLab CI jobs for GoLang projects.

## Contents
- [golang:<tag>](https://hub.docker.com/_/golang?tab=tags)
- [golint](https://github.com/golang/lint)
- [go-junit-report](https://github.com/jstemmer/go-junit-report)
- [bc](https://www.tecmint.com/bc-command-examples/)
- [any-badge](https://pypi.org/project/anybadge/)

## Make Docker image
1. Change directory to project's root directory
2. Run the commands below:
```bash
$ export IMAGE_TAG=hwsc/ci-go:1.1.4
$ export DOCKERFILE_PATH=dockerfiles/ci/go
# There are two options in building the image in order to make the base image and upgrade dynamically
# Option 1: defaults to ARG BASE_IMAGE in Dockerfile
$ docker build -f ${DOCKERFILE_PATH}/Dockerfile -t ${IMAGE_TAG} ${DOCKERFILE_PATH}/
# Option 2: to specify a different base image
$ docker build -f ${DOCKERFILE_PATH}/Dockerfile --build-arg BASE_IMAGE="golang:<major.minor.path>" -t ${IMAGE_TAG} ${DOCKERFILE_PATH}/
```

## Publish Docker image to Dockerhub
Run the commands below:
```bash
$ docker login
$ docker push ${image_tag}
```

## Changelog

### 1.1.4
#### Add
- add pycodestyle 2.6.0
#### Update
- change default base image to golang:1.14.3
- update from python 3.8.2 to 3.8.3

### 1.1.3
#### Update
- change default base image to golang:1.14.2
- update from python 3.8.1 to 3.8.2
- update from anybadge 1.6.2 to 1.7.0

### 1.1.2
#### Update
- change default base image to golang:1.14.1

### 1.1.1
#### Update
- change default base image to golang:1.14.0

### 1.1.0
#### Add
- build args support for dynamic base image
- wget
- python 3.8.1
- anybadge 1.6.2
- del_img.py for deleting Docker image(s) in DockerHub

### 1.0.0
#### Add
- base Docker image
