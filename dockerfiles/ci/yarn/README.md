# hwsc/ci-yarn Image
`hwsc/ci-yarn` image is used to run GitLab CI jobs for Yarn projects.

## Contents
- [node](https://hub.docker.com/_/node/)
- [any-badge](https://pypi.org/project/anybadge/)

## Make Docker image
1. Change directory to project's root directory
2. Run the commands below:
```bash
$ export IMAGE_TAG=hwsc/ci-yarn:1.0.0
$ export DOCKERFILE_PATH=dockerfiles/ci/yarn
# There are two options in building the image in order to make the base image and upgrade dynamically
# Option 1: defaults to ARG BASE_IMAGE in Dockerfile
$ docker build -f ${DOCKERFILE_PATH}/Dockerfile -t ${IMAGE_TAG} ${DOCKERFILE_PATH}/
# Option 2: to specify a different base image
$ docker build -f ${DOCKERFILE_PATH}/Dockerfile --build-arg BASE_IMAGE="node:<major.minor.path>" -t ${IMAGE_TAG} ${DOCKERFILE_PATH}/
```

## Publish Docker image to Dockerhub
Run the commands below:
```bash
$ docker login
$ docker push ${image_tag}
```

## Changelog
### 1.0.0
#### Add
- base Docker image
- node:12.16.2
- anybadge 1.7.0
- pip3
