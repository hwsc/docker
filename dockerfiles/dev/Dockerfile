FROM ubuntu:20.04

# WARNING: Use LTS only. If there is no LTS, then use stable releases.
ENV DL_DIR=/home/download
RUN mkdir -p /${DL_DIR}

# install utilities
RUN apt-get update && \
apt-get install -y \
    build-essential \
    zlib1g-dev \
    libncurses5-dev \
    libgdbm-dev \
    libnss3-dev \
    libssl-dev \
    libreadline-dev \
    libffi-dev \
    curl \
    wget \
    make \
    jq \
    gpg \
    xz-utils \
    git \
    gcc \
    g++

# install nodejs
RUN wget https://nodejs.org/dist/v12.17.0/node-v12.17.0-linux-x64.tar.xz -P ${DL_DIR}/ && \
tar -xf ${DL_DIR}/node-v12.17.0-linux-x64.tar.xz --directory /usr/local --strip-components 1

# install yarn
RUN wget https://github.com/yarnpkg/yarn/releases/download/v1.22.4/yarn_1.22.4_all.deb -P ${DL_DIR}/ && \
dpkg -i ${DL_DIR}/yarn_1.22.4_all.deb

# install golang
ENV GOLANG_VERSION=1.14.3
ENV GOPATH=/go
ENV PATH="/usr/local/go/bin:${GOPATH}/bin:${PATH}"
RUN wget https://storage.googleapis.com/golang/go1.14.3.linux-amd64.tar.gz -P ${DL_DIR}/ && \
tar -C /usr/local -xzf ${DL_DIR}/go1.14.3.linux-amd64.tar.gz && \
mkdir -p "$GOPATH/src" "$GOPATH/bin" && chmod -R 777 "$GOPATH" && \
go get -u github.com/golang/protobuf/proto && \
go get -u github.com/golang/protobuf/protoc-gen-go && \
go get -u google.golang.org/grpc && \
go get -u golang.org/x/lint/golint && \
go get -u github.com/jstemmer/go-junit-report && \
go get github.com/favadi/protoc-go-inject-tag

# install python
RUN wget https://www.python.org/ftp/python/3.8.3/Python-3.8.3.tgz -P ${DL_DIR}/ && \
tar -xf ${DL_DIR}/Python-3.8.3.tgz -C ${DL_DIR} && \
cd ${DL_DIR}/Python-3.8.3 && \
./configure --enable-optimizations && \
make -j 8 && \
make altinstall && \
pip3.8 install --upgrade pip  && \
pip3.8 install \
    'pipenv==2020.5.28' \
    'pycodestyle==2.6.0'

# install protoc
RUN wget https://github.com/protocolbuffers/protobuf/releases/download/v3.12.2/protobuf-all-3.12.2.tar.gz -P ${DL_DIR}/ && \
tar -zxvf ${DL_DIR}/protobuf-all-3.12.2.tar.gz -C ${DL_DIR} && \
cd ${DL_DIR}/protobuf-3.12.2/ && \
./configure && \
make && \
make check && \
make install && \
ldconfig

# install protoc-gen-grpc-web
RUN wget https://github.com/grpc/grpc-web/releases/download/1.1.0/protoc-gen-grpc-web-1.1.0-linux-x86_64 -P ${DL_DIR}/ && \
mv ${DL_DIR}/protoc-gen-grpc-web-1.1.0-linux-x86_64 /usr/local/bin/protoc-gen-grpc-web && \
chmod +x /usr/local/bin/protoc-gen-grpc-web
