# hwsc/dev Image
`hwsc/dev` image is used to support development in https://gitlab.com/hwsc/app 

## Contents
The image contains developer's environment and tools that are stated in [on-boarding document](https://hwsc.slab.com/posts/on-boarding-tjy2iks0).

## Make Docker image
1. Change directory to project's root directory
2. Run the commands below:
```bash
$ export IMAGE_TAG=hwsc/dev:1.0.0
$ export DOCKERFILE_PATH=dockerfiles/dev
$ docker build -f ${DOCKERFILE_PATH}/Dockerfile -t ${IMAGE_TAG} ${DOCKERFILE_PATH}/
```

## Publish Docker image to Dockerhub
Run the commands below:
```bash
$ docker login
$ docker push ${image_tag}
```

## Changelog
### 1.0.0
#### Add
- base Docker image
