# hwsc/docker Repo

## Purpose
This repo contains Dockerfiles, and Docker composition related files for managing projects and continuous integration (CI).

### Dev Image
`hwsc/dev` can be mounted to the current working directory to utilize tools and environment to build `hwsc/app` and compile protocol buffers.

## Contents
- [CI image for Go projects](https://gitlab.com/hwsc/docker/tree/master/dockerfiles/ci/go)
- [Yarn CI image](https://gitlab.com/hwsc/docker/-/tree/master/dockerfiles/ci/yarn)
- [Dev image](https://gitlab.com/hwsc/docker/-/tree/master/dockerfiles/dev)
